package com.financeapp.financeapprest.dto;

import lombok.Data;
import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

@Data
public class PrincipalDto {

    @NotNull
    @NotBlank
    @Length(min = 2, max = 50)
    private String username;

    @NotNull
    @NotBlank
    private String firstName;

    @NotNull
    @NotBlank
    private String lastName;

    @NotNull
    @NotBlank
    private String password;

    @NotNull
    @NotBlank
    private String phoneNumber;

    @NotNull
    @NotBlank
    private String emailAddress;

    @NotNull
    @NotBlank
    private String roleName;

    @NotNull
    @NotBlank
    private String departmentName;

}
