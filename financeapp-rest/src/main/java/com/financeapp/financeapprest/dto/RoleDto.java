package com.financeapp.financeapprest.dto;

import lombok.Data;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import java.util.ArrayList;
import java.util.List;

@Data
public class RoleDto {

    @NotNull
    @NotBlank
    private String name;

    @NotNull
    @NotBlank
    private String description;

    private List<String> privilegeNames = new ArrayList<>();

}
