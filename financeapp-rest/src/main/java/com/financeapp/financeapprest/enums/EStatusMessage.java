package com.financeapp.financeapprest.enums;

public enum EStatusMessage {

    FAILURE("FAILURE"),
    SUCCESS("SUCCESS"),
    UNVAILABLE("UNAVAILABLE"),
    INPROGRESS("INPROGRESS");

    private String description;

    EStatusMessage(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
