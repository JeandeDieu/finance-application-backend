package com.financeapp.financeapprest.enums;

public enum ELifeCycleState {

    ACTIVE("ACTIVE"),

    INACTIVE("INACTIVE");

    private final String description;


    ELifeCycleState(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
