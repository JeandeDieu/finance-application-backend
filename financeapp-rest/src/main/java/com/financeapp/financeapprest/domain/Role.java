package com.financeapp.financeapprest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.financeapp.financeapprest.enums.ELifeCycleState;
import lombok.Data;
import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "ROLE", uniqueConstraints = { @UniqueConstraint(columnNames = { "NAME" } )})
@Data
public final class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "ROLE_ID")
    private UUID roleId;

    @NotNull
    @NotBlank
    @Length(max = 250)
    @Column(name = "NAME", nullable = false, length = 250)
    private String name;

    @NotBlank
    @Length(max = 250)
    @Column(name = "DESCRIPTION", nullable = true, length = 250)
    private String description;

    @NotNull
    @Column(name = "STATE", nullable = false)
    private ELifeCycleState state;

    @JsonIgnore
    @ManyToMany(mappedBy = "roles")
    private List<Principal> principals;

    @ManyToMany
    @JoinTable(
            name = "ROLES_PRIVILEGES",
            joinColumns = @JoinColumn(
                    name = "ROLE_ID", referencedColumnName = "ROLE_ID"),
            inverseJoinColumns = @JoinColumn(
                    name = "PRIVILEGE_ID", referencedColumnName = "PRIVILEGE_ID"))
    private List<Privilege> privileges;

}
