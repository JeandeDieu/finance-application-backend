package com.financeapp.financeapprest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.financeapp.financeapprest.enums.ELifeCycleState;
import lombok.Data;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "PRIVILEGE", uniqueConstraints = { @UniqueConstraint(columnNames = { "NAME" } )})
@Data
public final class Privilege implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "PRIVILEGE_ID")
    private UUID privilegeId;

    @NotNull
    @NotBlank
    @Column(name = "NAME", length = 250)
    private String name;

    @NotNull
    @NotBlank
    @Column(name = "DESCRIPTION", length = 250)
    private String description;

    @NotNull
    @Column(name = "STATE", nullable = false)
    private ELifeCycleState state;

    @JsonIgnore
    @ManyToMany(mappedBy = "privileges")
    private List<Role> roles;

}
