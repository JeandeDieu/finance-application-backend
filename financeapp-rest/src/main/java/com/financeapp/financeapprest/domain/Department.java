package com.financeapp.financeapprest.domain;

import com.financeapp.financeapprest.enums.ELifeCycleState;
import lombok.Data;
import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "DEPARTMENT", uniqueConstraints = {@UniqueConstraint(columnNames = { "DEPARTMENT_NAME" } )})
@Data
public final class Department implements Serializable {

    public static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @NotNull
    @Column(name = "DEPARTMENT_ID", nullable = false)
    private UUID departmentId;

    @NotNull
    @NotBlank
    @Length(min = 2, max = 255)
    @Column(name = "DEPARTMENT_NAME", nullable = false, length = 255)
    private String departmentName;

    @Column(name = "ALIAS", nullable = true)
    private String alias;

    @Column(name = "DESCRIPTION")
    private String description;

    @NotNull
    @NotBlank
    @Column(name = "STATE")
    private ELifeCycleState state;

    @OneToMany(mappedBy = "department")
    private List<Principal> principal;
}
