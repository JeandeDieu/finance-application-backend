package com.financeapp.financeapprest.domain;

import com.financeapp.financeapprest.enums.ELifeCycleState;
import lombok.Data;
import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "PRINCIPAL", uniqueConstraints = { @UniqueConstraint(columnNames = {"USER_NAME"})})
@Data
public final class Principal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "PRINCIPAL_ID")
    private UUID principalId;

    @NotNull
    @NotBlank
    @Length(min = 2, max = 50)
    @Column(name = "USER_NAME", nullable = false, length = 50)
    private String username;

    @NotNull
    @NotBlank
    @Column(name = "FIRST_NAME", nullable = false)
    private String firstName;

    @NotNull
    @NotBlank
    @Column(name = "LAST_NAME", nullable = false)
    private String lastName;

    @NotNull
    @NotBlank
    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @NotNull
    @NotBlank
    @Column(name = "PHONE_NUMBER", nullable = false)
    private String phoneNumber;

    @NotNull
    @NotBlank
    @Column(name = "EMAIL_ADDRESS", nullable = false)
    private String emailAddress;

    @Column(name = "USER_IMAGE")
    private byte[] userImage;

    @NotNull
    @Column(name = "FIRST_LOGIN")
    private boolean isFirstLogin;

    @NotNull
    @Column(name = "STATE", nullable = false)
    private ELifeCycleState state;

    @ManyToMany
    @JoinTable(
            name = "PRINCIPALS_ROLES",
            joinColumns = @JoinColumn(
                    name = "PRINCIPAL_ID", referencedColumnName = "PRINCIPAL_ID"),
            inverseJoinColumns = @JoinColumn(
                    name = "ROLE_ID", referencedColumnName = "ROLE_ID"))
    private List<Role> roles;

    @ManyToOne
    private Department department;

}
