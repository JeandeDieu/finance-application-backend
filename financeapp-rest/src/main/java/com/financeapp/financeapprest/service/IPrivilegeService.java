package com.financeapp.financeapprest.service;

import com.financeapp.financeapprest.domain.Privilege;
import com.financeapp.financeapprest.dto.PrivilegeDto;
import org.springframework.http.ResponseEntity;


public interface IPrivilegeService {

    public ResponseEntity<Privilege> createPrivilege(PrivilegeDto privilegeDto);

}
