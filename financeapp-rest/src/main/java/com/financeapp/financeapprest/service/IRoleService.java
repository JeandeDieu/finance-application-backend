package com.financeapp.financeapprest.service;

import com.financeapp.financeapprest.domain.Privilege;
import com.financeapp.financeapprest.dto.RoleDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IRoleService {

    public ResponseEntity createRole(RoleDto roleDto);
}
