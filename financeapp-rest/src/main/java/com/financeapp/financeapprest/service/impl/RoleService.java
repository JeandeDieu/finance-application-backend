package com.financeapp.financeapprest.service.impl;

import com.financeapp.financeapprest.domain.Privilege;
import com.financeapp.financeapprest.domain.Role;
import com.financeapp.financeapprest.dto.RoleDto;
import com.financeapp.financeapprest.enums.ELifeCycleState;
import com.financeapp.financeapprest.enums.EStatusMessage;
import com.financeapp.financeapprest.repository.PrivilegeRepository;
import com.financeapp.financeapprest.repository.RoleRepository;
import com.financeapp.financeapprest.service.IRoleService;
import com.financeapp.financeapprest.util.FailureResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleService implements IRoleService{

    @Autowired
    private RoleRepository iRoleRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    FailureResponse failureResponse = new FailureResponse();

    @Override
    public ResponseEntity createRole(RoleDto roleDto){

        List<Privilege> rolePrivileges = new ArrayList<>();

        Role role = iRoleRepository.findByName(roleDto.getName());

        if (role == null) {
            role = new Role();
            role.setName(roleDto.getName());
            role.setDescription(roleDto.getDescription());
            role.setState(ELifeCycleState.ACTIVE);

            for(String name: roleDto.getPrivilegeNames()){
                Privilege privilege = privilegeRepository.findByName(name);
                if(privilege == null){
                    failureResponse.setErrorMessage("Privilege with Name "+name+" does not exist");
                    failureResponse.setErrorCode(HttpStatus.CONFLICT.toString());
                    failureResponse.setStatusMessage(EStatusMessage.FAILURE);
                    return new ResponseEntity(failureResponse, HttpStatus.CONFLICT);
                }else{
                    rolePrivileges.add(privilege);
                }
            }
            role.setPrivileges(rolePrivileges);
            Role response = iRoleRepository.save(role);

            return new ResponseEntity(response, HttpStatus.OK);
        }else{
            FailureResponse failureResponse = new FailureResponse();
            failureResponse.setErrorMessage("Role Name already exists");
            failureResponse.setErrorCode(HttpStatus.CONFLICT.toString());
            failureResponse.setStatusMessage(EStatusMessage.FAILURE);
            return new ResponseEntity(failureResponse, HttpStatus.CONFLICT);
        }
    }
}
