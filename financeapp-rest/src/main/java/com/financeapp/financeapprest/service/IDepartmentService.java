package com.financeapp.financeapprest.service;

import com.financeapp.financeapprest.domain.Department;
import org.springframework.http.ResponseEntity;

public interface IDepartmentService {

    public ResponseEntity<Department> createDepartment(Department department);
}
