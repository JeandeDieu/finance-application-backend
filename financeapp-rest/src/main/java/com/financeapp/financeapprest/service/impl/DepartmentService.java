package com.financeapp.financeapprest.service.impl;

import com.financeapp.financeapprest.domain.Department;
import com.financeapp.financeapprest.enums.ELifeCycleState;
import com.financeapp.financeapprest.repository.DepartmentRepository;
import com.financeapp.financeapprest.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class DepartmentService implements IDepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public ResponseEntity createDepartment(Department department) {
        try{
            department.setState(ELifeCycleState.ACTIVE);
            Department response = this.departmentRepository.save(department);
            return new ResponseEntity(response, HttpStatus.OK);
        }catch (Exception e){
            return ResponseEntity.unprocessableEntity().body(e.getLocalizedMessage());
        }
    }
}
