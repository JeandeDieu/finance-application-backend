package com.financeapp.financeapprest.service;

import com.financeapp.financeapprest.domain.Principal;
import com.financeapp.financeapprest.dto.PrincipalDto;
import org.springframework.http.ResponseEntity;

public interface IPrincipalService {

    public ResponseEntity<Principal> createPrincipal(PrincipalDto principalDto);
}
