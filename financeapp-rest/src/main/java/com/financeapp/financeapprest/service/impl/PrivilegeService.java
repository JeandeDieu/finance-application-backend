package com.financeapp.financeapprest.service.impl;

import com.financeapp.financeapprest.domain.Privilege;
import com.financeapp.financeapprest.dto.PrivilegeDto;
import com.financeapp.financeapprest.enums.ELifeCycleState;
import com.financeapp.financeapprest.enums.EStatusMessage;
import com.financeapp.financeapprest.repository.PrivilegeRepository;
import com.financeapp.financeapprest.service.IPrivilegeService;
import com.financeapp.financeapprest.util.FailureResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PrivilegeService implements IPrivilegeService {

    @Autowired
    private PrivilegeRepository privilegeRepository;

    FailureResponse failureResponse = new FailureResponse();

    @Override
    public ResponseEntity createPrivilege(PrivilegeDto privilegeDto) {

        Privilege privilege = privilegeRepository.findByName(privilegeDto.getName());

        if(privilege == null){
            privilege = new Privilege();
            privilege.setName(privilegeDto.getName());
            privilege.setDescription(privilegeDto.getDescription());
            privilege.setState(ELifeCycleState.ACTIVE);

            Privilege response = privilegeRepository.save(privilege);

            return new ResponseEntity<>(response, HttpStatus.OK);
        }else{
            failureResponse.setErrorCode(HttpStatus.CONFLICT.toString());
            failureResponse.setStatusMessage(EStatusMessage.FAILURE);
            failureResponse.setErrorMessage("Privilege already Exists");
            return new ResponseEntity<>(failureResponse, HttpStatus.CONFLICT);
        }
    }

}
