package com.financeapp.financeapprest.service.impl;

import com.financeapp.financeapprest.domain.Department;
import com.financeapp.financeapprest.domain.Principal;
import com.financeapp.financeapprest.domain.Role;
import com.financeapp.financeapprest.dto.PrincipalDto;
import com.financeapp.financeapprest.enums.ELifeCycleState;
import com.financeapp.financeapprest.enums.EStatusMessage;
import com.financeapp.financeapprest.repository.DepartmentRepository;
import com.financeapp.financeapprest.repository.PrincipalRepository;
import com.financeapp.financeapprest.repository.RoleRepository;
import com.financeapp.financeapprest.service.IPrincipalService;
import com.financeapp.financeapprest.util.FailureResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PrincipalService implements IPrincipalService {

    @Autowired
    private PrincipalRepository principalRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    FailureResponse failureResponse = new FailureResponse();

    @Override
    public ResponseEntity createPrincipal(PrincipalDto principalDto) {
        try {
            Role role = roleRepository.findByName(principalDto.getRoleName());

            Department department = departmentRepository.findByDepartmentName(principalDto.getDepartmentName());

            if (role == null) {
                failureResponse.setStatusMessage(EStatusMessage.FAILURE);
                failureResponse.setErrorCode(HttpStatus.NOT_FOUND.toString());
                failureResponse.setErrorMessage("Role Name Not Found");
                return new ResponseEntity<>(failureResponse, HttpStatus.NOT_FOUND);
            } else if(department == null){
                failureResponse.setStatusMessage(EStatusMessage.FAILURE);
                failureResponse.setErrorCode(HttpStatus.NOT_FOUND.toString());
                failureResponse.setErrorMessage("Department Name Not Found");
                return new ResponseEntity<>(failureResponse, HttpStatus.NOT_FOUND);
            } else{
                Principal principal = new Principal();
                BeanUtils.copyProperties(principal, principalDto);
                principal.setState(ELifeCycleState.ACTIVE);
                principal.setFirstLogin(true);
                Principal response = this.principalRepository.save(principal);
                return new ResponseEntity(response, HttpStatus.OK);
            }
        }catch (Exception e){
            return ResponseEntity.unprocessableEntity().body(e.getLocalizedMessage());
        }
    }
}
