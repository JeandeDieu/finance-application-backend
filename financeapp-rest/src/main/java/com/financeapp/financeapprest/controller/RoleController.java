package com.financeapp.financeapprest.controller;

import com.financeapp.financeapprest.dto.RoleDto;
import com.financeapp.financeapprest.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private IRoleService iRoleService;

    @PostMapping("/create")
    public ResponseEntity createRole(@RequestBody RoleDto roleDto){
        try {
            return iRoleService.createRole(roleDto);
        }catch(Exception e){
            return ResponseEntity.unprocessableEntity().body(e.getLocalizedMessage());
        }
    }
}
