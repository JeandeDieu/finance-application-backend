package com.financeapp.financeapprest.controller;

import com.financeapp.financeapprest.dto.PrivilegeDto;
import com.financeapp.financeapprest.service.IPrivilegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/privilege")
public class PrivilegeController {

    @Autowired
    private IPrivilegeService iPrivilegeService;

    @PostMapping("/create")
    public ResponseEntity createPrivilege(@RequestBody PrivilegeDto privilegeDto){
        try{
            return iPrivilegeService.createPrivilege(privilegeDto);
        }catch (Exception e){
            return ResponseEntity.unprocessableEntity().body(e.getLocalizedMessage());
        }
    }
}
