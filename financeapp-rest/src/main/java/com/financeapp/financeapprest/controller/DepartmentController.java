package com.financeapp.financeapprest.controller;

import com.financeapp.financeapprest.domain.Department;
import com.financeapp.financeapprest.repository.DepartmentRepository;
import com.financeapp.financeapprest.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private IDepartmentService iDepartmentService;

    @PostMapping("/create")
    public ResponseEntity createDepartment(@RequestBody Department department){
        try{
            return this.iDepartmentService.createDepartment(department);
        }catch (Exception e){
            return ResponseEntity.unprocessableEntity().body(e.getLocalizedMessage());
        }
    }
}
