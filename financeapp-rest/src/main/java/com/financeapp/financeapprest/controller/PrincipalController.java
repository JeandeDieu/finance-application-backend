package com.financeapp.financeapprest.controller;

import com.financeapp.financeapprest.dto.PrincipalDto;
import com.financeapp.financeapprest.service.IPrincipalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/principal")
public class PrincipalController {

    @Autowired
    private IPrincipalService iPrincipalService;

    @PostMapping("/create")
    public ResponseEntity createPrincipal(@RequestBody PrincipalDto principalDto){
        try{
            return this.iPrincipalService.createPrincipal(principalDto);
        }catch (Exception e){
            return ResponseEntity.unprocessableEntity().body(e.getLocalizedMessage());
        }
    }
}
