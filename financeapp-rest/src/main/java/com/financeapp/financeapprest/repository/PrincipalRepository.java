package com.financeapp.financeapprest.repository;

import com.financeapp.financeapprest.domain.Principal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PrincipalRepository extends JpaRepository<Principal, UUID> {

}
