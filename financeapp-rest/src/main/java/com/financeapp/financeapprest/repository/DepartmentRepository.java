package com.financeapp.financeapprest.repository;

import com.financeapp.financeapprest.domain.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, UUID> {

    public Department findByDepartmentName(String departmentName);
}
