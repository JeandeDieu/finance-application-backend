package com.financeapp.financeapprest.util;

import com.financeapp.financeapprest.enums.EStatusMessage;
import lombok.Data;

@Data
public class FailureResponse {

    private String errorCode;
    private String errorMessage;
    private EStatusMessage statusMessage;
}
