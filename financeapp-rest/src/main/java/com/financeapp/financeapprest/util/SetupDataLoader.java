package com.financeapp.financeapprest.util;

import com.financeapp.financeapprest.domain.Principal;
import com.financeapp.financeapprest.domain.Privilege;
import com.financeapp.financeapprest.domain.Role;
import com.financeapp.financeapprest.enums.ELifeCycleState;
import com.financeapp.financeapprest.repository.PrincipalRepository;
import com.financeapp.financeapprest.repository.PrivilegeRepository;
import com.financeapp.financeapprest.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

//@Component
public class SetupDataLoader {


    boolean alreadySetup = false;

    @Autowired
    private PrincipalRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

//    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (alreadySetup)
            return;
        Privilege readPrivilege
                = createPrivilegeIfNotFound("READ_PRIVILEGE");
        Privilege writePrivilege
                = createPrivilegeIfNotFound("WRITE_PRIVILEGE");

        List<Privilege> adminPrivileges = Arrays.asList(
                readPrivilege, writePrivilege);
        createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        createRoleIfNotFound("ROLE_USER", Arrays.asList(readPrivilege));

        Role adminRole = roleRepository.findByName("ROLE_ADMIN");
        Principal user = new Principal();
        user.setFirstName("Test");
        user.setLastName("Test");
        user.setPassword("test");
        user.setPhoneNumber("0788");
        user.setUsername("Test");
        user.setEmailAddress("test@test.com");
        user.setState(ELifeCycleState.ACTIVE);
        user.setRoles(Arrays.asList(adminRole));
        userRepository.save(user);

        alreadySetup = true;
    }

    @Transactional
    Privilege createPrivilegeIfNotFound(String name) {

        Privilege privilege = privilegeRepository.findByName(name);
        if (privilege == null) {
            privilege = new Privilege();
            privilege.setName(name);
            privilege.setDescription("Test");
            privilege.setState(ELifeCycleState.ACTIVE);
            privilegeRepository.save(privilege);
        }
        return privilege;
    }

    @Transactional
    Role createRoleIfNotFound(
            String name, List<Privilege> privileges) {

        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role();
            role.setName(name);
            role.setDescription("Test");
            role.setState(ELifeCycleState.ACTIVE);
            role.setPrivileges(privileges);
            roleRepository.save(role);
        }
        return role;
    }
}
