package com.financeapp.financeapprest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinanceappRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinanceappRestApplication.class, args);
	}

}
